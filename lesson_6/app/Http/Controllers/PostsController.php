<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;
use App\Models\Post;

class PostsController extends Controller
{
    public function show($slug)
    {
        //$post = DB::table('posts')->where('slug', $slug)->first();
        return view('post', [
            'post' => Post::where('slug', $slug)->firstOrFail()
        ]);

        /*
         * $post = Post::where('slug', $slug)->firstOrFail();

        return view('post', [
            'post' => $post
        ]);
         */
    }
}
