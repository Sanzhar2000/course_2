<?php

namespace App\Http\Controllers;

class PostsController
{
    public function show($posts)
    {
        $posts = [
            'my-first-post' => 'Hello, this is my first blog post',
            'my-second-post' => 'Now I am getting the hang of this blogging thing.'
        ];

        if(! array_key_exists($post, $posts)){
            abort(404, 'Sorry, that post not found');
        }
        return view('post', [
            'post' => $posts[$post] ?? 'Nothing here Yet'
        ]);
    }
}
